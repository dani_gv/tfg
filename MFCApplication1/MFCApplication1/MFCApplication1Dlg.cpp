
// MFCApplication1Dlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "MFCApplication1Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de di�logo CAboutDlg utilizado para el comando Acerca de

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Datos del cuadro de di�logo
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

	// Implementaci�n
protected:
	DECLARE_MESSAGE_MAP()
public:
	//afx_msg void OnTimer(UINT_PTR nIDEvent);
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	//ON_WM_TIMER()
END_MESSAGE_MAP()


// Cuadro de di�logo de CMFCApplication1Dlg



CMFCApplication1Dlg::CMFCApplication1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCApplication1Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCApplication1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//	ON_BN_CLICKED(IDC_BUTTON1, &CMFCApplication1Dlg::OnBnClickedButton1)
	//	ON_BN_CLICKED(IDC_BUTTON2, &CMFCApplication1Dlg::OnBnClickedButton2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_ApagarLuz, &CMFCApplication1Dlg::OnBnClickedApagarluz)
	ON_BN_CLICKED(IDC_EncenderLuz2, &CMFCApplication1Dlg::OnBnClickedEncenderluz2)
	ON_BN_CLICKED(IDC_LlamarEnfermeras, &CMFCApplication1Dlg::OnBnClickedLlamarenfermeras)
	ON_BN_CLICKED(IDC_BajarPersianas, &CMFCApplication1Dlg::OnBnClickedBajarpersianas)
	ON_BN_CLICKED(IDC_SubirPersianas, &CMFCApplication1Dlg::OnBnClickedSubirpersianas)
	ON_BN_CLICKED(IDC_AcercarBandeja, &CMFCApplication1Dlg::OnBnClickedAcercarbandeja)
	ON_BN_CLICKED(IDC_LevantarCama, &CMFCApplication1Dlg::OnBnClickedLevantarcama)
	ON_BN_CLICKED(IDC_BajarCama, &CMFCApplication1Dlg::OnBnClickedBajarcama)
	ON_BN_CLICKED(IDC_AlejarBandeja, &CMFCApplication1Dlg::OnBnClickedAlejarbandeja)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// Controladores de mensaje de CMFCApplication1Dlg

BOOL CMFCApplication1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// Agregar el elemento de men� "Acerca de..." al men� del sistema.

	// IDM_ABOUTBOX debe estar en el intervalo de comandos del sistema.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	X = 0.0f; 
	Y = 0.0f; 
	CWnd::SetTimer(1, 25, NULL);
	ShowCursor(false); 

	
	
	//pWnd1 = GetDlgItem(1000); 
	//pWnd2 = GetDlgItem(1001); 

	//pWnd1 -> GetWindowRect(&rect1); 
	//pWnd2 -> GetWindowRect(&rect2); 
	///////////////////////






	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

void CMFCApplication1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CMFCApplication1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CMFCApplication1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



//void CMFCApplication1Dlg::OnBnClickedButton1()
//{
//	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
//
//	/////////////////////Con s�mbolo de Warning
//	MessageBox((LPCTSTR)L"Has pulsado el boton Mensaje", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);
//
//	//AfxMessageBox(_T("Has pulsado el bot�n texto"));
//}


//void CMFCApplication1Dlg::OnBnClickedButton2()
//{
//	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
//
//	//Create(IDD_2, this); 
//	//ShowWindow(SW_SHOW); 
//
//	MessageBox((LPCTSTR)L"Has pulsado el boton Cambio", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);
//
//	//
////	ptestdlg = new CTestDialog(this);
////ptestdlg ->Create(CTestDialog::IDD,this);
////ptestdlg ->ShowWindow(SW_SHOW);
//
//}


void CMFCApplication1Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Agregue aqu� su c�digo de controlador de mensajes o llame al valor predeterminado


	////boton "Apagar Luz" pWnd1
	//CRect Apagar_Luz; 
	//CWnd *pWnd1 = GetDlgItem(1000);  ////1000 es el codigo del boton Apagar Luz
	//pWnd1 -> GetWindowRect(&Apagar_Luz); 

	////boton "Llamar Enfermeras" pWnd2
	//CRect Llamando_Enfermeras;
	//CWnd *pWnd2 = GetDlgItem(1001);   ////1001 es el codigo del boton Llamar Enfermeras
	//pWnd2 -> GetWindowRect(&Llamando_Enfermeras); 

	////boton "Encender Luz" pWnd3
	//CRect Encender_Luz;
	//CWnd *pWnd3 = GetDlgItem(1002);   ////1002 es el codigo del boton Encender Luz
	//pWnd3 -> GetWindowRect(&Encender_Luz); 

	////boton "Bajar Persianas" pWnd4
	//CRect Bajar_Persianas;
	//CWnd *pWnd4 = GetDlgItem(1003);   ////1003 es el codigo del boton Bajar Persianas
	//pWnd4 -> GetWindowRect(&Bajar_Persianas); 

	////boton "Subir Persianas" pWnd5
	//CRect Subir_Persianas;
	//CWnd *pWnd5 = GetDlgItem(1004);   ////1004 es el codigo del boton Subir Persianas
	//pWnd5 -> GetWindowRect(&Subir_Persianas); 

	////boton "Acercar Bandeja" pWnd6
	//CRect Acercar_Bandeja;
	//CWnd *pWnd6 = GetDlgItem(1005);   ////1005 es el codigo del boton Acercar Bandeja
	//pWnd6 -> GetWindowRect(&Acercar_Bandeja); 

	////boton "Levantar Cama" pWnd7
	//CRect Levantar_Cama;
	//CWnd *pWnd7 = GetDlgItem(1006);   ////1006 es el codigo del boton Levantar Cama
	//pWnd7 -> GetWindowRect(&Levantar_Cama); 

	////boton "Bajar Cama" pWnd8
	//CRect Bajar_Cama;
	//CWnd *pWnd8 = GetDlgItem(1007);   ////1007 es el codigo del boton Bajar Cama
	//pWnd8 -> GetWindowRect(&Bajar_Cama); 

	////boton "Alejar Bandeja" pWnd9
	//CRect Alejar_Bandeja;
	//CWnd *pWnd9 = GetDlgItem(1008);   ////1008 es el codigo del boton Alejar Bandeja
	//pWnd9 -> GetWindowRect(&Alejar_Bandeja); 


	CRect client_rect; 
	GetWindowRect(&client_rect); 
	ClipCursor(&client_rect); 


	_eyex.GetCoord(&X, &Y); 
	SetCursorPos((int)X, (int)Y); 


	if (_eyex.flanc_cierre())	{
		_eyex.Reset_flanco_cierre();
		OnClose(); 
	}
	

	///Coincide con algun boton!
	if ((_eyex.flanc_gui�o()) && (!_eyex.flanc_cierre()))	{ ////De esta manera le cuesta coger el gui�o
		_eyex.Reset_flanco_gui�o(); 

		/*	if (X!= 0.0f || Y!= 0.0f)	{ ////De esta manera coge s�lo los 3 � 4 primeros gui�os 
		_eyex.Reset_flanco(); */

		INPUT Input = {0}; 

		//left down
		Input.type = INPUT_MOUSE; 
		Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN; 
		::SendInput(1, &Input, sizeof(INPUT)); 

		//left up
		::ZeroMemory(&Input,sizeof(INPUT)); 
		Input.type = INPUT_MOUSE; 
		Input.mi.dwFlags = MOUSEEVENTF_LEFTUP; 
		::SendInput(1,&Input,sizeof(INPUT)); 



		//if ((X <= Apagar_Luz.right) & (X >= Apagar_Luz.left) & (Y >= Apagar_Luz.top) & (Y <=Apagar_Luz.bottom))	{
		//	OnBnClickedApagarluz(); 
		//}

		//else if ((X <= Llamando_Enfermeras.right) & (X >= Llamando_Enfermeras.left) & (Y >= Llamando_Enfermeras.top) & (Y <= Llamando_Enfermeras.bottom))	{
		//	OnBnClickedLlamarenfermeras(); 
		//}
		//else if ((X <= Encender_Luz.right) & (X >= Encender_Luz.left) & (Y >= Encender_Luz.top) & (Y <= Encender_Luz.bottom))	{
		//	OnBnClickedEncenderluz2(); 
		//}
		//else if ((X <= Bajar_Persianas.right) & (X >= Bajar_Persianas.left) & (Y >= Bajar_Persianas.top) & (Y <= Bajar_Persianas.bottom))	{
		//	OnBnClickedBajarpersianas();		
		//}
		//else if ((X <= Subir_Persianas.right) & (X >= Subir_Persianas.left) & (Y >= Subir_Persianas.top) & (Y <= Subir_Persianas.bottom))	{
		//	OnBnClickedSubirpersianas();		
		//}
		//else if ((X <= Acercar_Bandeja.right) & (X >= Acercar_Bandeja.left) & (Y >= Acercar_Bandeja.top) & (Y <= Acercar_Bandeja.bottom))	{
		//	OnBnClickedAcercarbandeja();		
		//}
		//else if ((X <= Levantar_Cama.right) & (X >= Levantar_Cama.left) & (Y >= Levantar_Cama.top) & (Y <= Levantar_Cama.bottom))	{
		//	OnBnClickedLevantarcama();		
		//}
		//else if ((X <= Bajar_Cama.right) & (X >= Bajar_Cama.left) & (Y >= Bajar_Cama.top) & (Y <= Bajar_Cama.bottom))	{
		//	OnBnClickedBajarcama();		
		//}
		//else if ((X <= Alejar_Bandeja.right) & (X >= Alejar_Bandeja.left) & (Y >= Alejar_Bandeja.top) & (Y <= Alejar_Bandeja.bottom))	{
		//	OnBnClickedAlejarbandeja();		
		//}




		///a�adir tantos 'else if' como botones haya


		//else
		//	MessageBox((LPCTSTR)L"Has pulsado el boton Cambio", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONERROR); 


		///Reiniciar los valores X e Y y establecer las coordenadas del tobii	

		/*X = 0.0f; 
		Y = 0.0f; */
		//_eyex.SetCoord(X, Y); 

	}

	//CRect rect1;
	//CWnd *pWnd1 = GetDlgItem(102); 
	//pWnd1 -> GetWindowRect(&rect1); 

	//LPPOINT point; 
	//GetCursorPos(point); 

	//if ((X >= rect1.right) || (X <= rect1.left) || (Y <= rect1.top) || (Y >= rect1.bottom))	{
	//	SetCursorPos(750,350); 
	//}


	//_eyex.Update_Gui�o(); 
	CDialogEx::OnTimer(nIDEvent);
}


void CMFCApplication1Dlg::OnBnClickedApagarluz()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Apagando Luz...", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedEncenderluz2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Encendiendo Luz habitacion...", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedLlamarenfermeras()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Llamando Enfermeras", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedBajarpersianas()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Bajando Persianas habitacion...", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedSubirpersianas()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Subiendo Persianas", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedAcercarbandeja()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Acercando la Bandeja de la comida...", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedLevantarcama()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Levantando la cama", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedBajarcama()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Bajando la altura de la cama", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnBnClickedAlejarbandeja()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	MessageBox((LPCTSTR)L"Alejando Bandeja...", (LPCTSTR)L"Mensaje", MB_OK|MB_ICONEXCLAMATION);

}


void CMFCApplication1Dlg::OnClose()
{
	// TODO: Agregue aqu� su c�digo de controlador de mensajes o llame al valor predeterminado
	if (MessageBox((LPCTSTR)L"�Desea cerrar la aplicacion?", (LPCTSTR)L"Cierre", MB_YESNO|MB_ICONQUESTION) == IDYES)
		//CDialogEx::OnClose();
		DestroyWindow(); 
}
