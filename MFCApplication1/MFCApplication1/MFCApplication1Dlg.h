
// MFCApplication1Dlg.h: archivo de encabezado
//

#pragma once

#include "EyeXhost.h"
#include <Windows.h>

// Cuadro de di�logo de CMFCApplication1Dlg
class CMFCApplication1Dlg : public CDialogEx
{
// Construcci�n
public:
	CMFCApplication1Dlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_MFCAPPLICATION1_DIALOG };



	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	EyeXhost _eyex; 
	float X; 
	float Y; 

	//CRect rect1; 
	//CWnd *pWnd1;
	//CRect rect2;
	//CWnd *pWnd2; 

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedButton1();
//	afx_msg void OnBnClickedButton2();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedApagarluz();
	afx_msg void OnBnClickedEncenderluz2();
	afx_msg void OnBnClickedLlamarenfermeras();
	afx_msg void OnBnClickedBajarpersianas();
	afx_msg void OnBnClickedSubirpersianas();
	afx_msg void OnBnClickedAcercarbandeja();
	afx_msg void OnBnClickedLevantarcama();
	afx_msg void OnBnClickedBajarcama();
	afx_msg void OnBnClickedAlejarbandeja();
	afx_msg void OnClose();
};
