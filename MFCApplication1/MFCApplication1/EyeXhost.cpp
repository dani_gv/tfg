#include "stdafx.h"
#include "EyeXhost.h"


//Para ver la posicion de los ojos con respecto al centro de la pantalla		1
//Para ver d�nde estoy mirando													2



#pragma comment (lib, "Tobii.EyeX.Client.lib")

// ID of the global interactor that provides our data stream; must be unique within the application.
static const TX_STRING InteractorId = "Rainbow Dash";

// global variables
static TX_HANDLE g_hGlobalInteractorSnapshot = TX_EMPTY_HANDLE;




void TX_CALLCONVENTION miSnapshot(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
	EyeXhost *aux=static_cast<EyeXhost *>(param);
	aux->OnSnapshotCommitted(hAsyncData,param);
}
void TX_CALLCONVENTION miHandle(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
	EyeXhost *aux=static_cast<EyeXhost *>(param);
	aux->HandleEvent(hAsyncData,param);
}
void TX_CALLCONVENTION miEngine(TX_CONNECTIONSTATE hAsyncData, TX_USERPARAM param)
{
	EyeXhost *aux=static_cast<EyeXhost *>(param);
	aux->OnEngineConnectionStateChanged(hAsyncData,param);
}


EyeXhost::EyeXhost(void) : Gui�o(FALSE), Gui�o_prev(FALSE), flanco_gui�o(FALSE), testigo(0), flanco_cierre(FALSE), cierre (FALSE), testigo_2(0)
{
	SetCoord(0.0f, 0.0f); 
	time1 = new SYSTEMTIME; 
	time2 = new SYSTEMTIME; 
	time3 = new SYSTEMTIME; 



	hContext = TX_EMPTY_HANDLE;
	TX_TICKET hConnectionStateChangedTicket = TX_INVALID_TICKET;
	TX_TICKET hEventHandlerTicket = TX_INVALID_TICKET;

	// initialize and enable the context that is our link to the EyeX Engine.
	success1 = txInitializeEyeX(TX_EYEXCOMPONENTOVERRIDEFLAG_NONE, NULL, NULL, NULL, NULL) == TX_RESULT_OK;
	success1 &= txCreateContext(&hContext, TX_FALSE) == TX_RESULT_OK;
	success1 &= InitializeGlobalInteractorSnapshot(hContext);
	success1 &= txRegisterConnectionStateChangedHandler(hContext, &hConnectionStateChangedTicket, miEngine, this) == TX_RESULT_OK;
	success1 &= txRegisterEventHandler(hContext, &hEventHandlerTicket, miHandle, this) == TX_RESULT_OK;
	success1 &= txEnableConnection(hContext) == TX_RESULT_OK;

}


EyeXhost::~EyeXhost(void)
{
	txDisableConnection(hContext);
	txReleaseObject(&g_hGlobalInteractorSnapshot);
	success1 = txShutdownContext(hContext, TX_CLEANUPTIMEOUT_DEFAULT, TX_FALSE) == TX_RESULT_OK;
	success1 &= txReleaseContext(&hContext) == TX_RESULT_OK;
	success1 &= txUninitializeEyeX() == TX_RESULT_OK;
}


/*
* Initializes g_hGlobalInteractorSnapshot with an interactor that has the Fixation Data behavior.
*/
BOOL EyeXhost::InitializeGlobalInteractorSnapshot(TX_CONTEXTHANDLE hContext)
{
	TX_HANDLE hInteractor = TX_EMPTY_HANDLE;

	//1
	TX_HANDLE hBehaviorWithoutParameters = TX_EMPTY_HANDLE;
	//2
	TX_FIXATIONDATAPARAMS params = { TX_FIXATIONDATAMODE_SENSITIVE };

	//General
	BOOL success;

	success = txCreateGlobalInteractorSnapshot(
		hContext,
		InteractorId,
		&g_hGlobalInteractorSnapshot,
		&hInteractor) == TX_RESULT_OK;
	//1
	success &= txCreateInteractorBehavior(hInteractor, &hBehaviorWithoutParameters, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK;
	//2
	success &= txCreateFixationDataBehavior(hInteractor, &params) == TX_RESULT_OK;

	//General
	txReleaseObject(&hInteractor);

	return success;

}

/*
* Callback function invoked when a snapshot has been committed.
*/
void EyeXhost::OnSnapshotCommitted(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
	// check the result code using an assertion.
	// this will catch validation errors and runtime errors in debug builds. in release builds it won't do anything.

	TX_RESULT result = TX_RESULT_UNKNOWN;
	txGetAsyncDataResultCode(hAsyncData, &result);
	assert(result == TX_RESULT_OK || result == TX_RESULT_CANCELLED);
}

/*
* Callback function invoked when the status of the connection to the EyeX Engine has changed.
*/
void EyeXhost::OnEngineConnectionStateChanged(TX_CONNECTIONSTATE connectionState, TX_USERPARAM userParam)
{
	switch (connectionState) {
	case TX_CONNECTIONSTATE_CONNECTED: {
		BOOL success;
		//		printf("The connection state is now CONNECTED (We are connected to the EyeX Engine)\n");
		// commit the snapshot with the global interactor as soon as the connection to the engine is established.
		// (it cannot be done earlier because committing means "send to the engine".)
		success = txCommitSnapshotAsync(g_hGlobalInteractorSnapshot, miSnapshot, this) == TX_RESULT_OK;
		if (!success) {
			//			printf("Failed to initialize the data stream.\n");
		}
		else {
			//			printf("Waiting for eye position data to start streaming...\n");
		}
									   }
									   break;

	case TX_CONNECTIONSTATE_DISCONNECTED:
		//	printf("The connection state is now DISCONNECTED (We are disconnected from the EyeX Engine)\n");
		break;

	case TX_CONNECTIONSTATE_TRYINGTOCONNECT:
		//	printf("The connection state is now TRYINGTOCONNECT (We are trying to connect to the EyeX Engine)\n");
		break;

	case TX_CONNECTIONSTATE_SERVERVERSIONTOOLOW:
		//	printf("The connection state is now SERVER_VERSION_TOO_LOW: this application requires a more recent version of the EyeX Engine to run.\n");
		break;

	case TX_CONNECTIONSTATE_SERVERVERSIONTOOHIGH:
		//	printf("The connection state is now SERVER_VERSION_TOO_HIGH: this application requires an older version of the EyeX Engine to run.\n");
		break;
	}
}

/*
* Callback function invoked when an event has been received from the EyeX Engine.
*/
void EyeXhost::HandleEvent(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam)
{
	TX_HANDLE hEvent = TX_EMPTY_HANDLE;
	TX_HANDLE hBehavior1 = TX_EMPTY_HANDLE;
	TX_HANDLE hBehavior2 = TX_EMPTY_HANDLE;


	txGetAsyncDataContent(hAsyncData, &hEvent);

	// NOTE. Uncomment the following line of code to view the event object. The same function can be used with any interaction object.
	//OutputDebugStringA(txDebugObject(hEvent));

	//1
	if (txGetEventBehavior(hEvent, &hBehavior1, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK) {
		Click(hBehavior1);
		txReleaseObject(&hBehavior1);
	}



	//2
	//	if (flanc_gui�o())	{
	if (txGetEventBehavior(hEvent, &hBehavior2, TX_BEHAVIORTYPE_FIXATIONDATA) == TX_RESULT_OK) {
		Position(hBehavior2);
		txReleaseObject(&hBehavior2);
	}
	//	}



	// NOTE since this is a very simple application with a single interactor and a single data stream, 
	// our event handling code can be very simple too. A more complex application would typically have to 
	// check for multiple behaviors and route events based on interactor IDs.


	txReleaseObject(&hEvent);
}


void EyeXhost::Click(TX_HANDLE hEyePositionDataBehavior)	{
	COORD position = {0,8};
	TX_EYEPOSITIONDATAEVENTPARAMS eventParams;

	if (txGetEyePositionDataEventParams(hEyePositionDataBehavior, &eventParams) == TX_RESULT_OK)	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);

		///////////////Cierre ojos
		if ((eventParams.RightEyeX == (double) 0.0f) && (eventParams.RightEyeY == (double) 0.0f) && (eventParams.RightEyeZ == (double) 0.0f))	{   
			if ((eventParams.LeftEyeX != (double) 0.0f) && (eventParams.LeftEyeY != (double) 0.0f) && (eventParams.LeftEyeZ != (double) 0.0f))	{  ///Ojo derecho gui�ado
//		if ((eventParams.LeftEyeX == (double) 0.0f) && (eventParams.LeftEyeY == (double) 0.0f) && (eventParams.LeftEyeZ == (double) 0.0f))	{   
//			if ((eventParams.RightEyeX != (double) 0.0f) && (eventParams.RightEyeY != (double) 0.0f) && (eventParams.RightEyeZ != (double) 0.0f))	{ 


				if ((Gui�o_prev == FALSE) && testigo == 0)	{
					GetSystemTime(time1); 
					testigo++; 
					Gui�o_prev = TRUE; 
				}
			}	
			else ///ojos cerrados
			{
				if (cierre == FALSE && testigo_2==0)
				{
					GetSystemTime(time3); 
					cierre = TRUE; 
					//testigo = 0;
					testigo_2++; 
				}
			}
		}
		else ////Ojo derecho abierto
		{
			if ((eventParams.LeftEyeX != (double) 0.0f) && (eventParams.LeftEyeY != (double) 0.0f) && (eventParams.LeftEyeZ != (double) 0.0f))	{ //// Ojos abiertos
//			if ((eventParams.RightEyeX != (double) 0.0f) && (eventParams.RightEyeY != (double) 0.0f) && (eventParams.RightEyeZ != (double) 0.0f))	{ //// Ojos abiertos
				if ((Gui�o_prev == TRUE) && (testigo==1) /*&& (cierre == FALSE)/*&& (testigo_2 == 0)*/)	{
					GetSystemTime(time2); 
					testigo++; 
				}
				if (cierre == TRUE && testigo_2 == 1)	{
					GetSystemTime(time2); 
					testigo_2++; 
				}
				Gui�o_prev = FALSE; 
				cierre = FALSE; 
			}
			else if ((eventParams.LeftEyeX == (double) 0.0f) && (eventParams.LeftEyeY == (double) 0.0f) && (eventParams.LeftEyeZ == (double) 0.0f))  //// Ojo izquiero gui�ado
//			else if ((eventParams.RightEyeX == (double) 0.0f) && (eventParams.RightEyeY == (double) 0.0f) && (eventParams.RightEyeZ == (double) 0.0f))  //// Ojo izquiero gui�ado
			{		
			}
		}

		//////////////Comprobaci�n gui�os con tiempos
		//	if ((time2->wSecond >= 0) && (time2->wSecond < 60) && (time3->wSecond >= 0) && (time3->wSecond < 60))	{
		if (testigo_2 == 2)	{
			if ((((time2->wSecond - time3->wSecond) * 1000) + (time2->wMilliseconds - time3->wMilliseconds)) >= 1500)	{
				flanco_cierre = TRUE; 
				testigo = 0;
			}
			time2->wSecond = 60; 
			time3->wSecond = 60; 
			testigo_2 = 0; 
		}

		//if (((time2->wMilliseconds) > 0) && ((time2->wMilliseconds) < 1000) && ((time1->wMilliseconds) > 0) && ((time1->wMilliseconds) < 1000))	{
		if (testigo == 2)	{
			if ((time2->wMilliseconds) < (time1->wMilliseconds))		{			     /////Si tiempo 1 y tiempo 2 se dan en segundos diferentes
				if ((((time2->wMilliseconds) + 1000) - (time1->wMilliseconds)) >= 200)	{    ////Si hay gui�o
					flanco_gui�o = TRUE; 
					time2->wMilliseconds = 0; 
					time1->wMilliseconds = 0; 
				}
			}
			else
			{
				if (((time2->wMilliseconds) - (time1->wMilliseconds)) >= 200)	{ 
					flanco_gui�o = TRUE;  
					time2->wMilliseconds = 0; 
					time1->wMilliseconds = 0; 
				}
			}	
			testigo = 0; 
		}
	}
}

void EyeXhost::Position(TX_HANDLE hFixationDataBehavior)	{

	TX_FIXATIONDATAEVENTPARAMS eventParams;

	if (txGetFixationDataEventParams(hFixationDataBehavior, &eventParams) == TX_RESULT_OK)
		SetCoord((float)eventParams.X, (float)eventParams.Y); 
}

void EyeXhost::SetCoord(float X, float Y)	{
	coord.X = X; 
	coord.Y = Y; 
	return; 
}

void EyeXhost::GetCoord(float *X, float *Y)	{
	(*X) = coord.X; 
	(*Y) = coord.Y; 
	return; 
}

bool EyeXhost::flanc_gui�o()	{
	return flanco_gui�o; 
}

void EyeXhost::Update_Gui�o()	{
	Gui�o_prev = Gui�o; 
}

void EyeXhost::Reset_flanco_gui�o()	{
	flanco_gui�o = FALSE; 
}

bool EyeXhost::flanc_cierre()	{
	return flanco_cierre; 
}

void EyeXhost::Reset_flanco_cierre()	{
	flanco_cierre = FALSE; 
}