#pragma once

#include "include\eyex\EyeX.h"
#include <time.h>
#include <Windows.h>



class EyeXhost
{
public:
	EyeXhost(void);
	~EyeXhost(void);

	void /*TX_CALLCONVENTION*/ HandleEvent(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam);
	void /*TX_CALLCONVENTION*/ OnSnapshotCommitted(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param);
	void /*TX_CALLCONVENTION*/ OnEngineConnectionStateChanged(TX_CONNECTIONSTATE connectionState, TX_USERPARAM userParam);

	void SetCoord (float CoordX, float CoordY); 
	void GetCoord(float *CoordX, float *CoordY); 

	bool flanc_gui�o(); 
	void Update_Gui�o(); 
	void Reset_flanco_gui�o();
	bool flanc_cierre(); 
	void Reset_flanco_cierre(); 


protected: 
	TX_CONTEXTHANDLE hContext; 
	BOOL success1;


	BOOL InitializeGlobalInteractorSnapshot(TX_CONTEXTHANDLE hContext);
	void Click(TX_HANDLE hBehavior); 
	void Position(TX_HANDLE hFixationDataBehavior); 

	bool Gui�o; 
	bool Gui�o_prev; 
	bool flanco_gui�o; 
	bool flanco_cierre;
	bool cierre; 
	SYSTEMTIME *time1; 
	SYSTEMTIME *time2; 
	SYSTEMTIME *time3; 
	int testigo; 
	int testigo_2; 


	typedef struct {
		float X; 
		float Y; 
	}Coordenadas; 
	Coordenadas coord; 
};

